import random
import pygame

SCREEN_WIDTH = 650
SCREEN_HEIGHT = 420

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Memory by Ivan Jurisic")
clock = pygame.time.Clock()
pygame.font.init()

positions_list = [[(60.0, 10.0)], [(192.0, 10.0)], [(324.0, 10.0)], [(456.0, 10.0)], [(60.0, 142.0)], [(192.0, 142.0)], [(324.0, 142.0)], [(456.0, 142.0)], [(60.0, 274.0)], [(192.0, 274.0)]]
image_list = [pygame.image.load("pozadina.jpg"), pygame.image.load("slika_1.jpg"), pygame.image.load("slika_2.jpg"), pygame.image.load("slika_3.jpg"), pygame.image.load("slika_4.jpg"), pygame.image.load("slika_5.jpg")]
opened_cards_list = []
memory_card_list = []

IMG_WIDTH = 130
IMG_HEIGHT = 130

over = False
current_state = "start"
score = 0
opened_card_1 = None
opened_card_2 = None
wait_counter = 1000

while not over:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			over = True
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_SPACE:
				current_state = "set_grid"
				score = 0
			elif event.key == pygame.K_ESCAPE:
				if current_state == "set_grid" or current_state == "show_cards":
					current_state = "ending"

		elif event.type == pygame.MOUSEBUTTONDOWN:
			if current_state == "show_cards":
				for card in memory_card_list:
					if card[1].collidepoint(event.pos):
						card[2] = True
						if opened_card_1 == None:
							opened_card_1 = card[3]
						elif opened_card_2 == None:
							opened_card_2 = card[3]
						break

	screen.fill((255, 255, 255))
	if current_state == "start":
		font = pygame.font.SysFont("Arial", 22, False)
		render = font.render("Hej! Dobrodosao u moju igru.", False, (0, 0, 0))
		screen.blit(render,((SCREEN_WIDTH/2) - 120.0, (SCREEN_HEIGHT/2) - 50.0))
		font = pygame.font.SysFont("Arial", 18, False)
		render = font.render("Za start klikni SPACE", False, (0, 0, 0))
		screen.blit(render,((SCREEN_WIDTH/2) - 70.0, (SCREEN_HEIGHT/2) - 20.0))
		

	elif current_state == "set_grid":
		firstList = []
		index = 0
		for image in image_list:
			if index > 0:
				img = pygame.transform.scale(image, (IMG_WIDTH, IMG_HEIGHT))
				firstList.append([img, None, False, index])
			index += 1

		secondList = []
		for image in firstList:
			secondList.append([image[0], None, False, image[3]])

		memory_card_list = firstList + secondList
		print(opened_cards_list)
		random.shuffle(memory_card_list)
		current_state = "show_cards"

	elif current_state == "show_cards":	
		font = pygame.font.SysFont("Arial", 22, False)
		render = font.render("REZULTAT: " + str(score), False, (0, 0, 0))
		screen.blit(render, (SCREEN_WIDTH - 250.0, SCREEN_HEIGHT - 85.0))
		
		i = 0
		for card in memory_card_list:
			img = card[0]
			if card[2] == False:
				img = pygame.transform.scale(image_list[0], (IMG_WIDTH, IMG_HEIGHT))

			card[1] = screen.blit(img, positions_list[i][0])
			i = i + 1

		if opened_card_1 is not None and opened_card_2 is not None:
			wait_counter = 800
			current_state = "waiting"

	elif current_state == "card_check":
		font = pygame.font.SysFont("Arial", 22, False)
		render = font.render("REZULTAT: " + str(score), False, (0, 0, 0))
		screen.blit(render, (SCREEN_WIDTH - 250.0, SCREEN_HEIGHT - 85.0))

		i = 0
		for card in memory_card_list:
			img = card[0]
			if card[2] == False:
				img = pygame.transform.scale(image_list[0], (IMG_WIDTH, IMG_HEIGHT))

			card[1] = screen.blit(img, positions_list[i][0])
			i = i + 1

		if opened_card_1 != opened_card_2:
			for card in memory_card_list:
				if card[3] not in opened_cards_list:
					card[2] = False
		else:
			score = score + 1

			if score % 2 == 0:
				random.shuffle(memory_card_list)

			opened_cards_list.append(opened_card_1)
			opened_cards_list.append(opened_card_2)

		opened_card_1 = None
		opened_card_2 = None

		if len(opened_cards_list) == len(memory_card_list):
			current_state = "set_grid"
			del opened_cards_list[:]
		else:
			current_state = "show_cards"

	elif current_state == "waiting":
		font = pygame.font.SysFont("Arial", 22, False)
		render = font.render("REZULTAT: " + str(score), False, (0, 0, 0))
		screen.blit(render, (SCREEN_WIDTH - 250.0, SCREEN_HEIGHT - 85.0))
		
		i = 0
		for card in memory_card_list:
			img = card[0]
			if card[2] == False:
				img = pygame.transform.scale(image_list[0], (IMG_WIDTH, IMG_HEIGHT))

			card[1] = screen.blit(img, positions_list[i][0])
			i = i + 1

		wait_counter -= clock.get_time()
		if wait_counter <= 0:
			current_state = "card_check"
			wait_counter = 800

	elif current_state == "ending":
		font = pygame.font.SysFont("Arial", 22, False)
		render = font.render("Zavrsio si igru! Tvoj rezultat je " + str(score), False, (0, 0, 0))
		screen.blit(render,((SCREEN_WIDTH/2) - 120.0, (SCREEN_HEIGHT/2) - 50.0))
		font = pygame.font.SysFont("Arial", 18, False)
		render = font.render("Za start klikni SPACE", False, (0, 0, 0))
		screen.blit(render,((SCREEN_WIDTH/2) - 70.0, (SCREEN_HEIGHT/2) - 20.0))

	pygame.display.flip()
	clock.tick(60)